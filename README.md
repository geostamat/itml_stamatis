The application connects to Drupal 8 Database, and queries the body_value
column of the node_revision__body table, for a term provided by the user

Dependencies

jre-9.0.4
mysql-connector-java-5.1.46-bin.jar