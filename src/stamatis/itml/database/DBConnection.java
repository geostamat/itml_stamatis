/**
 * Class that handles the connection to the database and
 * the statements/queries
 */

package stamatis.itml.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class DBConnection {
	
	private final String USERNAME = "root";
	private final String PASSWORD = "root";
	private final String DBADDRESS = "localhost:3306/itmldrupal8";
	private final String TABLENAME = "node_revision__body";
	private final String COLUMNNAME = "body_value";
	
	/**
	 * Creates the connection to the database
	 * @return
	 * @throws SQLException
	 * @throws ClassNotFoundException 
	 */
	private Connection createConnection(){
		Connection conn = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://"+ DBADDRESS +
	                "?user=" + USERNAME + "&password=" + PASSWORD);
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Error connecting to database: " + e.toString());
			//e.printStackTrace();
		} 		
		return conn;
	}
	
	/**
	 * Returns a HashMap of the results, where the key is a concatenation of the fields
	 * that the primary key consists of, and the value is the content of body_value
	 * column
	 * @param term String that the method will look for in the body_value column
	 * @return
	 */
	public HashMap<String, String> getQuery(String term){
		HashMap<String, String> results = new HashMap<String, String>();
		try (Connection conn = createConnection();) {
			PreparedStatement statement = conn.prepareStatement("SELECT * FROM " + TABLENAME 
					+ " WHERE " + COLUMNNAME + " REGEXP ?");
			statement.setString(1, "[[:<:]]" + term + "[[:>:]]");
			ResultSet rs = statement.executeQuery();
			results = convertResultSet(rs);
			rs.close();
			conn.close();
		} catch (SQLException e) {
			System.out.println("Error querying the database: " + e.toString());
			//e.printStackTrace();
		}
		return results;
	}
	
	
	/**
	 * Converts the ResultSet into a HashMap, where the key is a concatenation of the fields
	 * that the primary key consists of, and the value is the content of body_value
	 * @param rs
	 * @return
	 * @throws SQLException 
	 */
	private HashMap<String, String> convertResultSet(ResultSet rs) throws SQLException{
		HashMap<String, String> results = new HashMap<String, String>();
		while(rs.next()) {
			//Primary key consists of columns deleted, entity_id, revision_id, langcode and delta
			results.put(rs.getByte("deleted") + " " + rs.getInt("entity_id") + " " + rs.getInt("revision_id") 
			+ " " + rs.getString("langcode") + " " + rs.getInt("delta"), rs.getString("body_value"));
		}
		return results;
	}
	
	/**
	 * Takes HashMap of the form <Concatenated Primary Key, New Value for body_value> and stores them
	 * in the database
	 * @param newResults
	 * @return
	 */
	public boolean storeResults(HashMap<String, String> newResults) {
		PreparedStatement statement= null;
		for(String s: newResults.keySet()) {
			//split the key in order to get the fields for each column of the primary key
			String[] primaryKey = s.split(" ");
			try (Connection conn = createConnection();){
				statement = conn.prepareStatement("UPDATE " + TABLENAME + " SET " + COLUMNNAME 
						+ " = ? WHERE deleted=? AND entity_id=? AND revision_id=? AND langcode=? AND delta=?");
				statement.setString(1, newResults.get(s));
				statement.setString(2, primaryKey[0]);
				statement.setString(3, primaryKey[1]);
				statement.setString(4, primaryKey[2]);
				statement.setString(5, primaryKey[3]);
				statement.setString(6, primaryKey[4]);
				statement.executeUpdate();				
				conn.close();
			} catch (SQLException e) {
				//System.out.println("Error Storing results");
				e.printStackTrace();
				return false;
			}
		}
		System.out.println("Changes succesfully saved");
		return true;
	}
	
	
}
