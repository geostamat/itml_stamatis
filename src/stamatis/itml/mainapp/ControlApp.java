/**
 * Class handles user input, and displays results on console
 */

package stamatis.itml.mainapp;

import java.util.HashMap;
import java.util.Scanner;

import stamatis.itml.database.DBConnection;

public class ControlApp {
	
	private HashMap<String, String> results;
	private Scanner scan = new Scanner(System.in);
	private DBConnection db = new DBConnection();
	//change variable below to false to make case insensitive replacements
	private final boolean ISCASESENSITIVE = true;
	
	public ControlApp() {
		userInterface();
	}
	
	public void userInterface() {
		//get input from user
		String term = getTerm();
		if(results.size() !=0) {
			String replacingTerm = getReplacingTerm(term);
			if(replacingTerm.length() != 0) {
				replaceTerm(term, replacingTerm);
			}
		}else{
			System.out.println("Term not found");
		}
		
		scan.close();
	}
	
	
	/**
	 * Method that asks the user for the term to search for, displays the results
	 * and stores them in a HashMap.
	 * @return Returns the term the user offered
	 */
	private String getTerm() {
		//ask user for term to search
		System.out.println("Which term whould you like to search for?");
		String term = scan.nextLine();
		//query database
		this.results = db.getQuery(term);
		displayResults();
		return term;
	}
	
	/**
	 * Method that asks the user for a new term to replace the original one
	 * @param term
	 * @return
	 */
	private String getReplacingTerm(String term) {
		//ask user for term to replace the original one with
		System.out.println("Which term whould you like to replace " + term + " with? "
				+ "Leave empty if you do not want \"" + term + "\" to be replaced.");
		if(ISCASESENSITIVE) {
			System.out.println("Replacements are case sensitive");
		}else {
			System.out.println("Replacements are NOT case sensitive");
		}
		String replacingTerm = scan.nextLine();
		return replacingTerm;
	}
	
	/**
	 * Displays the contents of results HashMap
	 */
	private void displayResults() {
		for(String key : results.keySet()) {
			//comment out line below to prevent primary key from displaying
			System.out.print(key + " ");
			System.out.println(results.get(key));
		}
	}
	
	/**
	 * Replaces the term in the results with the replacingTerm and stores them
	 * @param term
	 * @param replacingTerm
	 */
	private void replaceTerm(String term, String replacingTerm) {
		for(String s: results.keySet()) {
			if(ISCASESENSITIVE) {
				results.replace(s, results.get(s).replaceAll(term, replacingTerm));
			}else {
				results.replace(s, results.get(s).replaceAll("\\b(?i)" + term + "\\b", replacingTerm));
			}
			
			db.storeResults(results);
		}
		displayResults();
	}
}
